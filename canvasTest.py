from Tkinter import *
import Tkinter as tk

FILENAME = 'DrinkPic.gif'
root = Tk()
canvas = Canvas(root, width=1000, height=1250)
canvas.pack()
tk_img = PhotoImage(file = FILENAME)
canvas.create_image(0, 0, image=tk_img)
quit_button = Button(root, text = "Quit", command = root.quit, anchor = 'w',
                    width = 10, activebackground = "#33B5E5")
quit_button_window = canvas.create_window(10, 10, anchor='nw', window=quit_button)    
root.mainloop()
