from Tkinter import *
import Tkinter as tk
import sys
import tkFont
#import serial
#import time
import os


creds = 'tempfile.temp' # This just sets the variable creds to 'tempfile.temp'
#ser = serial.Serial('/dev/ttyACM0', 9600)
#time.sleep(1)

polyGUI= Tk()
polyGUI.title('POLYTELIA')
polyGUI.geometry("840x480+0+0")
mainfont = ('times', 60, 'bold', 'italic')
mainTitle = Label(polyGUI, text = 'Polytelia')
mainTitle.config(font=mainfont)
mainTitle.pack()            

def shot():

    global Shot
    global shotVal
    
    Shot = Toplevel()
    Shot.geometry("300x200+0+0")
    shotFont=('times', 12, 'bold')
    shotlabel = Label(Shot, text='Please select number of shots:\n')
    shotlabel.config(font= shotFont)
    shotlabel.pack()

    shotVal=IntVar()
    shotVal.set(0)
    radio1 = Radiobutton(Shot,text = 'Single',value = 1, variable=shotVal, command = Shot.destroy).pack(side='top')
    radio1 = Radiobutton(Shot,text = 'Double',value = 2, variable=shotVal, command = Shot.destroy).pack(side='bottom')
    
    Shot.mainloop() 

def settingsMenu():

    global smenu
    
    smenu = Tk()
    smenu.title('SETTING MENU')
    smenu.geometry("840x480+0+0")
    instrucFont=('times', 25, 'bold')
    instruction = Label(smenu, text='PLEASE SELECT A SETTINGS OPTION:\n')
    instruction.config(font= instrucFont)
    instruction.pack()

    

    smenu.mainloop()

     
def kidsMenu():

    global kmenu
    
    kmenu = Tk()
    kmenu.title('KID MENU')
    kmenu.geometry("840x480+0+0")
    instrucFont=('times', 25, 'bold')
    instruction = Label(kmenu, text='PLEASE SELECT A BEVERAGE:\n')
    instruction.config(font= instrucFont)
    instruction.pack()

    def appleJuice():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("1,5,5\n")
         #ser.close()
         print 'Apple Juice'
         kmenu.destroy()
         
    APbutton = Button(kmenu,text = 'Apple Juice',bg='#00ffff', height=5, width=10, command = appleJuice).place(x=30, y=210)

    kmenu.mainloop()


def adultMenu():
    
    global amenu
    
    amenu = Tk()
    amenu.title('ADULT MENU')
    amenu.geometry("840x480+0+0")
    instrucFont=('times', 25, 'bold')
    instruction = Label(amenu, text='PLEASE SELECT A BEVERAGE:\n')
    instruction.config(font= instrucFont)
    instruction.pack()

    #amenu.mainloop()
     

    def jackCoke():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("2,5,2,1,2\n")
         #ser.close()
         print 'Jack and coke'
         amenu.destroy()


    def whiskey():

        shot()
        shotCheck = shotVal.get()
        
        if shotCheck == 1:
            #ser.isOpen()
            #time.sleep(1)
            #time.sleep(1)
            #print ser.write("1,5,2\n")
            #ser.close()
            print'1shotwhiskey'
            amenu.destroy()
           
            
        if shotCheck == 2:
            #ser.isOpen()
            #time.sleep(1)
            #time.sleep(1)
            #print ser.write("1,5,4\n")
            #ser.close()
            print'2shotwhiskey'
            amenu.destroy()


    def jack():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("2,5,2,1,2\n")
         #ser.close()
         print 'Jack'
         amenu.destroy() 
            

    
    Jbutton = Button(amenu,text = 'Jack',bg='#00ffff', height=5, width=10, command = jack).place(x=370, y=210)
    JCbutton = Button(amenu,text = 'Jack and Coke',bg='#00ffff', height=5, width=10, command = jackCoke).place(x=30, y=210)
    Whiskeybutton = Button(amenu,text = 'Whiskey',bg='#00ffff', height=5, width=10, command = whiskey).place(x=200, y=210)
    amenu.mainloop()


        
def Login():
    global nameEL
    global pwordEL 
    global rootA
 
    rootA = Tk() # This now makes a new window.
    rootA.title('Login') # This makes the window title 'login'
 
    intruction = Label(rootA, text='Please Login\n') # More labels to tell us what they do
    intruction.grid(sticky=E) 
 
    nameL = Label(rootA, text='Username: ') 
    pwordL = Label(rootA, text='Password: ') 
    nameL.grid(row=1, sticky=W)
    pwordL.grid(row=2, sticky=W)
 
    nameEL = Entry(rootA) # The entry input
    pwordEL = Entry(rootA, show='*')
    nameEL.grid(row=1, column=1)
    pwordEL.grid(row=2, column=1)
 
    loginB = Button(rootA, text='Login', command=CheckLogin) # This makes the login button, which will go to the CheckLogin def.
    loginB.grid(columnspan=2, sticky=W)
 
    canCel = Button(rootA, text='Cancel', fg='red', command=Cancel) # This makes the deluser button. blah go to the deluser def.
    canCel.grid(columnspan=2, sticky=W)
    rootA.mainloop()

def Cancel():
    rootA.quit()
    rootA.destroy()
    
    
def CheckLogin():
    with open(creds) as f:
        data = f.readlines() # This takes the entire document we put the info into and puts it into the data variable
        uname = data[0].rstrip() # Data[0], 0 is the first line, 1 is the second and so on.
        pword = data[1].rstrip() # Using .rstrip() will remove the \n (new line) word from before when we input it
 
    if nameEL.get() == uname and pwordEL.get() == pword: # Checks to see if you entered the correct data.
        rootA.quit()
        rootA.destroy()
        adultMenu()
        
    else:
        r = Tk() # Opens new window
        r.geometry('150x50') # Makes the window a certain size
        rlbl = Label(r, text='\nFailed Login') # "logged in" label
        rlbl.pack() # Pack is like .grid(), just different
        r.mainloop()
        #nameEL.delete(0,'end')
        #pwordEL.delete(0,'end')
        Login()
         

labelfont=('times', 25, 'bold')
settingsbutton = Button(text = 'Settings',font = labelfont, bg='#00ffff', height=5, width=10, command = settingsMenu).place(x=30, y=210)
kidbutton = Button(text = 'Kid''s Menu', font = labelfont, bg='#00ffff', height=5, width=10, command = kidsMenu).place(x=300, y=210)
adultbutton = Button(text = 'Adult Menu',font = labelfont, bg='#00ffff', height=5, width=10, command = Login).place(x=570, y=210)
quitbutton = Button(text = 'QUIT', height=2, width=10, command = polyGUI.destroy). place(x=660,y=6)

polyGUI.mainloop()
