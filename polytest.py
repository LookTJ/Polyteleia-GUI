from Tkinter import *
import Tkinter as tk
import tkMessageBox
import sys
import tkFont
#import serial
#import time
import os

f1 = "DrinkPic.gif"
f2 = "Marg.gif"
f3 = "seven.gif"
f4 = "BTS.gif"
f5 = "Cape.gif"
f6 = "JCoke.gif"
f7 = "Kami.gif"
f8 = "Ltea.gif"
f9 = "Ofash.gif"
f10 = "Sbeach.gif"
f11 = "Tsun.gif"
f12 = "apple.gif"
f13 = "orange.gif"
f14 = "coke.gif"
f15 = "pineapp.gif"

creds = 'tempfile.temp'
cred2 = 'tempfile2.temp'# This just sets the variable creds to 'tempfile.temp'
#ser = serial.Serial('/dev/ttyACM0', 9600)
#time.sleep(1)

global polyGUI

polyGUI= Tk()
polyGUI.title('POLYTELIA')
polyGUI.geometry("840x480+0+0")


mainfont = ('times', 60, 'bold', 'italic')
canvas = Canvas(polyGUI, width=840, height=480)
canvas.pack()
tk_img = PhotoImage(file = f1)
canvas.create_image(420,250, image=tk_img)
canvas.create_text(400,40, font=mainfont, text ='Polyteleia')

def settingsMenu():

    global smenu
    
    smenu = Toplevel()
    smenu.title('SETTING MENU')
    smenu.geometry("840x480+0+0")
    instrucFont=('times', 25, 'bold')
    instruction = Label(smenu, text='PLEASE SELECT A SETTINGS OPTION:\n')
    instruction.config(font= instrucFont)
    instruction.pack()

    def ShutDown():
        smenu.destroy()

    def NewUser():
        Signup()
        #smenu.destroy()

    def NewPassword():
        ChangePass()
        #smenu.destroy()

    def DeleteUser():
        UserWindow()
        #smenu.destroy()

    Settingsfont=('times', 25, 'bold')
    
    newbutton = Button(smenu, text = 'New User',font = Settingsfont,  height=4, width=10, command = NewUser).place(x=30, y=80)
    passbutton = Button(smenu,text = 'Password\nReset', font = Settingsfont,  height=4, width=10, command = NewPassword).place(x=300, y=80)
    deletebutton = Button(smenu,text = 'Delete\nUser',font = Settingsfont, height=4, width=10, command = DeleteUser).place(x=570, y=80)
    quitbutton = Button(smenu,text = 'System\nShutdown', font = Settingsfont, bg='red', height=4, width=10, command = polyGUI.destroy). place(x=300,y=280)
    mainbutton = Button(smenu,text = 'Main\nMenu', height=2, width=10, command = smenu.destroy).place(x=570,y=400)

    
    
    smenu.mainloop()

     
def kidsMenu():

    global kmenu
    
    kmenu = Toplevel()
    kmenu.title('KID MENU')
    kmenu.geometry("840x480+0+0")
    instrucFont=('times', 25, 'bold')
    instruction = Label(kmenu, text='PLEASE SELECT A BEVERAGE:\n')
    instruction.config(font= instrucFont)
    instruction.pack()

    def appleJuice():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("1,10,5\n")
         #ser.close()
         print 'Apple Juice'
         kmenu.destroy()

    def orangeJuice():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("1,11,5\n")
         #ser.close()
         print 'Orange Juice'
         kmenu.destroy()

    def coke():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("1,12,5\n")
         #ser.close()
         print 'Coke'
         kmenu.destroy()

    def pineJuice():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("1,13,5\n")
         #ser.close()
         print 'Pineapple Juice'
         kmenu.destroy()

    appPic = PhotoImage(file = f12)
    appbutton = Button(kmenu,text = 'Apple Juice',image = appPic, justify=CENTER,compound= TOP, command = appleJuice).place(x = 30,y=180)
    
    orgPic = PhotoImage(file = f13)
    orgbutton = Button(kmenu,text = 'Orange Juice',image = orgPic, justify=CENTER,compound= TOP, command = orangeJuice).place(x = 230,y=180)
    
    cokePic = PhotoImage(file = f14)
    cokebutton = Button(kmenu,text = 'Coca-Cola',image = cokePic, justify=CENTER,compound= TOP, command = coke).place(x = 430,y=180)
    
    pinePic = PhotoImage(file = f15)
    pinebutton = Button(kmenu,text = 'Pineapple Juice',image = pinePic, justify=CENTER,compound= TOP, command = pineJuice).place(x = 630,y=180)     

    kmenu.mainloop()


def adultMenu():
    
    global amenu
    
    amenu = Toplevel()
    amenu.title('ADULT MENU')
    amenu.geometry("840x480+0+0")
    instrucFont=('times', 25, 'bold')
    instruction = Label(amenu, text='PLEASE SELECT A BEVERAGE:\n')
    instruction.config(font= instrucFont)
    instruction.pack()

    #amenu.mainloop()
     

    def TequilaSunrise():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("3,2,3,12,1,13,1\n")
         #ser.close()
         print 'Tequila Sunrise'
         amenu.destroy()

    def Margarita():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("4,2,2,10,2,4,1,13,1\n")
         #ser.close()
         print 'Margarita'
         amenu.destroy()

    def LongIsland():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("7,2,1,4,1,11,1,6,1,12,1,8,1,10,1\n")
         #ser.close()
         print 'Long Island'
         amenu.destroy()

    def SexOnBeach():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("4,8,1.25,12,1.25,6,1,10,1\n")
         #ser.close()
         print 'Sex on the beach'
         amenu.destroy()

    def CapeCod():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("2,2,2.25,10,2\n")
         #ser.close()
         print 'Cape Cod'
         amenu.destroy()

    def SevenAndSeven():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("2,6,2,10,2\n")
         #ser.close()
         print '7 and 7'
         amenu.destroy()

    def Kamikaze():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("3,8,2,12,1,11,1\n")
         #ser.close()
         print 'Kamikaze'
         amenu.destroy()
    

    def BwtSheets():
         #ser.isOpen()
         #time.sleep(1)
         #time.sleep(1)
         #print ser.write("4,4,2,11,1,6,2,13,1\n")
         #ser.close()
         print 'Between the Sheets'
         amenu.destroy()

    def JackCoke():

        shot()
        shotCheck = shotVal.get()
        
        if shotCheck == 1:
            #ser.isOpen()
            #time.sleep(1)
            #time.sleep(1)
            #print ser.write("2,6,2,12,2\n")
            #ser.close()
            print'Single Jack and Coke'
            
        else: 
            #ser.isOpen()
            #time.sleep(1)
            #time.sleep(1)
            #print ser.write("2,6,4,12,2\n")
            #ser.close()
            print'Double Jack and Coke'
            
        Shot.destroy()    
        amenu.destroy()

    def OldFashion():

        shot()
        shotCheck = shotVal.get()
        
        if shotCheck == 1:
            #ser.isOpen()
            #time.sleep(1)
            #time.sleep(1)
            #print ser.write("1,6,3\n")
            #ser.close()
            print'1shotwhiskey'
            
        else: 
            #ser.isOpen()
            #time.sleep(1)
            #time.sleep(1)
            #print ser.write("1,6,4.5\n")
            #ser.close()
            print'2shotwhiskey'
            
        Shot.destroy()    
        amenu.destroy()
            

    
    can = Canvas(amenu, width = 800, height =700)        
    menuframe = Frame(can, width = 825, height =700, bd =2, bg ="white", relief =SUNKEN)
    scrollbar = Scrollbar(amenu, orient="vertical", command=can.yview)
    can.configure(yscrollcommand=scrollbar.set)
    scrollbar.pack(side="right", fill= "y")
    can.pack()
    can.create_window((400,400), window=menuframe, tags="menuframe")
    task=can.configure(scrollregion=can.bbox("all"))
    menuframe.bind(task)

    
    Marg_pic = PhotoImage(file = f2)
    Margbutton = Button(menuframe,text = 'Margarita',image = Marg_pic, justify=CENTER,compound= TOP, command = Margarita).place(x = 30,y=10)
    
    sevenpic= PhotoImage(file = f3)
    sevenbutton = Button(menuframe,text = '7 and 7',image = sevenpic, justify=CENTER,compound= TOP, command = SevenAndSeven).place(x = 230,y=10)
    
    BTSpic = PhotoImage(file = f4)
    BTSbutton = Button(menuframe,text = 'Between the Sheets',image = BTSpic, justify=CENTER,compound= TOP, command = BwtSheets).place(x = 430,y=10)
    
    Capepic = PhotoImage(file = f5)
    Capebutton = Button(menuframe,text = 'Cape Cod',image = Capepic, justify=CENTER,compound= TOP, command = CapeCod).place(x = 630,y=10)
    
    Jcokepic = PhotoImage(file = f6)
    Jcokebutton = Button(menuframe,text = 'Jack and Coke',image = Jcokepic, justify=CENTER,compound= TOP, command = JackCoke).place(x = 30,y=220)
    
    Kamipic = PhotoImage(file = f7)
    Kamibutton = Button(menuframe,text = 'Kamikaze',image = Kamipic, justify=CENTER,compound= TOP, command = Kamikaze).place(x = 230,y=220)
    
    Ltea_pic = PhotoImage(file = f8)
    Lteabutton = Button(menuframe,text = 'Long Island Iced Tea',image = Ltea_pic, justify=CENTER,compound= TOP, command = LongIsland).place(x = 430,y=220)
    
    Ofash_pic = PhotoImage(file = f9)
    Ofashbutton = Button(menuframe,text = 'Old Fashion',image = Ofash_pic, justify=CENTER,compound= TOP, command = OldFashion).place(x = 630,y=220)
    
    SBpic = PhotoImage(file = f10)
    SBbutton = Button(menuframe,text = 'Sex on the Beach',image = SBpic, justify=CENTER,compound= TOP, command = SexOnBeach).place(x = 30,y=430)
    
    Tsun_pic = PhotoImage(file = f11)
    Tsunbutton = Button(menuframe,text = 'Tequila Sunrise',image = Tsun_pic, justify=CENTER,compound= TOP, command = TequilaSunrise).place(x = 230,y=430)
    
    amenu.mainloop()

def Signup(): # This is the signup definition, 
    global pwordE # These globals just make the variables global to the entire script, meaning any definition can use them
    global nameE
    global roots
 
    roots = Tk() # This creates the window, just a blank one.
    roots.title('Signup') # This renames the title of said window to 'signup'
    intruction = Label(roots, text='Please Enter new Credidentials\n') # This puts a label, so just a piece of text saying 'please enter blah'
    intruction.grid(row=0, column=0, sticky=E) # This just puts it in the window, on row 0, col 0. If you want to learn more look up a tkinter tutorial :)
 
    nameL = Label(roots, text='New Username: ') # This just does the same as above, instead with the text new username.
    pwordL = Label(roots, text='New Password: ') # ^^
    nameL.grid(row=1, column=0, sticky=W) # Same thing as the instruction var just on different rows. :) Tkinter is like that.
    pwordL.grid(row=2, column=0, sticky=W) # ^^
 
    nameE = Entry(roots) # This now puts a text box waiting for input.
    pwordE = Entry(roots, show='*') # Same as above, yet 'show="*"' What this does is replace the text with *, like a password box :D
    nameE.grid(row=1, column=1) # You know what this does now :D
    pwordE.grid(row=2, column=1) # ^^
 
    signupButton = Button(roots, text='Signup', command= FSSignup) # This creates the button with the text 'signup', when you click it, the command 'fssignup' will run. which is the def
    signupButton.grid(columnspan=2, sticky=W)
    roots.mainloop() # This just makes the window keep open, we will destroy it soon
 
def FSSignup():
    with open(cred2, 'w') as f: # Creates a document using the variable we made at the top.
        f.write(nameE.get()) # nameE is the variable we were storing the input to. Tkinter makes us use .get() to get the actual string.
        f.write('\n') # Splits the line so both variables are on different lines.
        f.write(pwordE.get()) # Same as nameE just with pword var
        f.close() # Closes the file
 
    roots.destroy() # This will destroy the signup window. :)
        
def Login():
    global nameEL
    global pwordEL 
    global rootA
 
    rootA = Tk() # This now makes a new window.
    rootA.title('Login') # This makes the window title 'login'
 
    intruction = Label(rootA, text='Please Login\n') # More labels to tell us what they do
    intruction.grid(sticky=E) 
 
    nameL = Label(rootA, text='Username: ') 
    pwordL = Label(rootA, text='Password: ') 
    nameL.grid(row=1, sticky=W)
    pwordL.grid(row=2, sticky=W)
 
    nameEL = Entry(rootA) # The entry input
    pwordEL = Entry(rootA, show='*')
    nameEL.grid(row=1, column=1)
    pwordEL.grid(row=2, column=1)
 
    loginB = Button(rootA, text='Login', command=CheckLogin) # This makes the login button, which will go to the CheckLogin def.
    loginB.grid(columnspan=2, sticky=W)
 
    canCel = Button(rootA, text='Cancel', fg='red', command=Cancel) # This makes the deluser button. blah go to the deluser def.
    canCel.grid(columnspan=2, sticky=W)
    rootA.mainloop()

def Cancel():
    rootA.quit()
    rootA.destroy()
    
    
def CheckLogin():
    with open(creds) as f:
        data = f.readlines() # This takes the entire document we put the info into and puts it into the data variable
        uname = data[0].rstrip() # Data[0], 0 is the first line, 1 is the second and so on.
        pword = data[1].rstrip() # Using .rstrip() will remove the \n (new line) word from before when we input it
 
    if nameEL.get() == uname and pwordEL.get() == pword: # Checks to see if you entered the correct data.
        rootA.quit()
        rootA.destroy()
        adultMenu()
        
    else:
        CheckLogin2()
        
def CheckLogin2():
    with open(cred2) as f:
        data = f.readlines() # This takes the entire document we put the info into and puts it into the data variable
        uname = data[0].rstrip() # Data[0], 0 is the first line, 1 is the second and so on.
        pword = data[1].rstrip() # Using .rstrip() will remove the \n (new line) word from before when we input it
 
    if nameEL.get() == uname and pwordEL.get() == pword: # Checks to see if you entered the correct data.
        rootA.quit()
        rootA.destroy()
        adultMenu()
        
    else:
        r = Tk() # Opens new window
        r.geometry('150x50') # Makes the window a certain size
        rlbl = Label(r, text='\nFailed Login') # "logged in" label
        rlbl.pack() # Pack is like .grid(), just different
        r.mainloop()
        #nameEL.delete(0,'end')
        #pwordEL.delete(0,'end')
        Login()

def UserWindow():
    
    global nameEL2
    global pwordEL2
    global rootD
 
    rootD = Tk() # This now makes a new window.
    rootD.title('Delete User') # This makes the window title 'login'
 
    intruction = Label(rootD, text='Enter User for Deletion:\n') # More labels to tell us what they do
    intruction.grid(sticky=E) 
 
    nameL = Label(rootD, text='Username: ') 
    pwordL = Label(rootD, text='Password: ') 
    nameL.grid(row=1, sticky=W)
    pwordL.grid(row=2, sticky=W)
 
    nameEL2 = Entry(rootD) # The entry input
    pwordEL2 = Entry(rootD, show='*')
    nameEL2.grid(row=1, column=1)
    pwordEL2.grid(row=2, column=1)
 
    loginB = Button(rootD, text='Delete', command=DelUser) # This makes the login button, which will go to the CheckLogin def.
    loginB.grid(columnspan=2, sticky=W)

    canCel = Button(rootD, text='Cancel', fg='red', command=rootD.destroy) # This makes the deluser button. blah go to the deluser def.
    canCel.grid(columnspan=2, sticky=W)

    rootD.mainloop()

    
def DelUser():
    
    os.remove(cred2) # Removes the file
    rootD.destroy() # Destroys the login window
    tkMessageBox.showwarning("Warning","User has been deleted")

def ChangePass():
    
    global nameEL3
    global pwordEL3
    global rootP
 
    rootP = Tk() # This now makes a new window.
    rootP.title('New Password') # This makes the window title 'login'
 
    intruction = Label(rootP, text='Enter Credentials\n') # More labels to tell us what they do
    intruction.grid(sticky=E) 
 
    nameL = Label(rootP, text='Username: ') 
    pwordL = Label(rootP, text='Password: ') 
    nameL.grid(row=1, sticky=W)
    pwordL.grid(row=2, sticky=W)
 
    nameEL3 = Entry(rootP) # The entry input
    pwordEL3 = Entry(rootP, show='*')
    nameEL3.grid(row=1, column=1)
    pwordEL3.grid(row=2, column=1)
 
    loginB = Button(rootP, text='Enter', command=updatePass) # This makes the login button, which will go to the CheckLogin def.
    loginB.grid(columnspan=2, sticky=W)

    canCel = Button(rootP, text='Cancel', fg='red', command=rootP.destroy) # This makes the deluser button. blah go to the deluser def.
    canCel.grid(columnspan=2, sticky=W)

    rootP.mainloop()

def updatePass():
    global pword1
    global pword2
    global rootP1

    def check():

        if pword1.get() == pword2.get():
            with open(cred2, 'w') as f: # Creates a document using the variable we made at the top.
                f.write(nameEL3.get()) # nameE is the variable we were storing the input to. Tkinter makes us use .get() to get the actual string.
                f.write('\n') # Splits the line so both variables are on different lines.
                f.write(pword1.get())
                f.close()
                rootP.destroy()
                rootP1.destroy()
        else:
            ChangePass()
            rootP1.quit()

 
    rootP1 = Tk() # This now makes a new window.
    rootP1.title('New Password') # This makes the window title 'login'
 
    intruction = Label(rootP1, text='Enter Credentials\n') # More labels to tell us what they do
    intruction.grid(sticky=E) 
 
    pword1 = Label(rootP1, text='Password: ') 
    pword2 = Label(rootP1, text='Password: ') 
    pword1.grid(row=1, sticky=W)
    pword2.grid(row=2, sticky=W)
 
    pword1 = Entry(rootP1,show='*') # The entry input
    pword2 = Entry(rootP1, show='*')
    pword1.grid(row=1, column=1)
    pword2.grid(row=2, column=1)
 
    loginB = Button(rootP1, text='Enter', command=check) # This makes the login button, which will go to the CheckLogin def.
    loginB.grid(columnspan=2, sticky=W)

    canCel = Button(rootP1, text='Cancel', fg='red', command=rootP1.destroy) # This makes the deluser button. blah go to the deluser def.
    canCel.grid(columnspan=2, sticky=W)
    
        
        
    rootP1.mainloop()
    
def shot():

    global Shot
    global shotVal
    
    Shot = Toplevel() 
    Shot.geometry("300x200+200+200")
    shotFont=('times', 12, 'bold')
    shotlabel = Label(Shot, text='Please select number of shots:\n')
    shotlabel.config(font= shotFont)
    shotlabel.pack()

    shotVal=IntVar()
    shotVal.set(0)
    radio1 = Radiobutton(Shot,text = 'Single\nShot',value = 1, variable=shotVal, command= Shot.quit).pack(side='top')
    radio2 = Radiobutton(Shot,text = 'Double\nShot',value = 2, variable=shotVal, command= Shot.quit).pack(side='bottom')
    
    Shot.mainloop() 

labelfont=('times', 25, 'bold')
settingsbutton = Button(text = 'Settings',font = labelfont, bg='red', height=5, width=10, command = settingsMenu).place(x=30, y=210)
kidbutton = Button(text = 'Kid''s Menu', font = labelfont, bg='blue', height=5, width=10, command = kidsMenu).place(x=300, y=210)
adultbutton = Button(text = 'Adult Menu',font = labelfont, bg='yellow', height=5, width=10, command = Login).place(x=570, y=210)

polyGUI.mainloop()
