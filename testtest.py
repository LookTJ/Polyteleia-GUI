from Tkinter import *
import Tkinter as tk
from ttk import *
import tkFont
#import serial
import time


#ser = serial.Serial ('/dev/ttyACM0', 9600)



dgui = Tk()

#Progress Bar
##separator = Frame(dgui, height=30, width=800, bd=1)
##separator.pack(side="bottom",padx=0, pady=4)
##progressbarlabel = Label(separator, text='Drink Progress').pack(side="left")
##progressbar = ttk.Progressbar(separator, orient=HORIZONTAL, length=400, mode='determinate')
##progressbar.pack(side="left",padx=0, pady=0)


#GUI Window Setup


dgui.title("Drink Motizer")
dgui.attributes('-fullscreen', True)
dgui.geometry('1024x600+100+50')


labelfont = ('times', 28, 'bold', 'italic')
selectfont = ('times', 10, 'italic')
##widget = Label(dgui, text='Drink Motizer')
##widget.config(font=labelfont)           
##widget.config(height=3, width=20)       
##widget.pack(expand=YES, fill=BOTH)
##widget.place(x=-15,y=-42)

subwidget = Label(dgui, text='Select Drink:')
subwidget.config(font=selectfont)
subwidget.place(x=290,y=31)

menuframe = Canvas(dgui, width=700, height=512, bd=2, bg="white", relief=SUNKEN)
menuframe.place(x=50,y=50)
#scrollbar = Scrollbar(menuframe)
#scrollbar.pack(side=RIGHT,fill=Y)
#menuframe.config(yscrollcommand=scrollbar)

def settingsmenu():
     smenu = Toplevel()
     smenu.title('Settings')
     smenu.geometry('600x400+300+100')
     smenu.iconbitmap(bitmap='Drink.ico')
     
#Main

settingsbutton = Button(text = 'Settings', height=2, width=10, command = settingsmenu). place(x=660,y=6)

Drinkfont = ('times', 12, 'bold', 'italic')
ingredfont = ('times', 9)
#Caribou Lou

'''
caribloupic = PhotoImage(file="cariboulou.gif")
caribloupicsave = Label(image=caribloupic)
caribloupicsave.image = caribloupic
caribloupicsave.place(in_=menuframe, x=20,y=20)
cbltitle = Label(menuframe,text='Caribou Lou',bg="white")
cbltitle.config(font=Drinkfont)
cbltitle.place(x=20,y=108)
cblouingred = Label(menuframe,text='151, Malibou, Pineapple Juice',bg="white").place(x=20,y=130)
cbloubutton = Button(menuframe,text = 'Make Drink', height=3, width=12).place(x=20,y=150)

#White Russian
WRpic = PhotoImage(file="whiterussian.gif")
WRpicsave = Label(image=WRpic)
WRpicsave.image = WRpic
WRpicsave.place(in_=menuframe, x=200,y=20)
WRtitle = Label(menuframe,text='White Russian',bg="white")
WRtitle.config(font=Drinkfont)
WRtitle.place(x=200,y=113)
WRingred = Label(menuframe,text='Vodka, Kaluha, Milk',bg="white").place(x=200,y=132)
WRbutton = Button(menuframe,text = 'Make Drink', height=3, width=12).place(x=200,y=152)
'''



def ButtonPress():
     progressbar.start()
     
def MT():
    progressbar.start()
 #   ser.open()

    time.sleep(1)
  #  ser.setDTR(level=0)
    time.sleep(1)

   # print ser.write('5,1,0,1,1,1,0,500,300')
     
def TC():
    progressbar.start()
    #ser.open()

    time.sleep(1)
    #ser.setDTR(level=0)
    time.sleep(1)

    #print ser.write('4,1,0,1,0,1,0,200,700')     
     
def CM():
    progressbar.start()
    #ser.open()

    time.sleep(1)
    #ser.setDTR(level=0)
    time.sleep(1)

    #print ser.write('4,1,0,1,0,1,0,200,700')     

def MH():
    progressbar.start()
    #ser.open()

    time.sleep(1)
    #ser.setDTR(level=0)
    time.sleep(1)

    #print ser.write('4,1,0,1,0,1,0,200,700')

#Button Press
def VR():
    progressbar.start()
    #ser.open()

    time.sleep(1)
    #ser.setDTR(level=0)
    time.sleep(1)

    #print ser.write('4,1,0,1,0,1,0,200,700')
   

#TP Button

TPb=Button(menuframe, justify = CENTER, bd=3,command=ButtonPress)
#TPpic = PhotoImage(file="teqsunrise.gif")
TPb.config(width="130",height="130",compound=TOP)
TPb.config(text="Tequila Sunrise", font=Drinkfont)
TPb.place(x=20,y=12)
TPingred = Label(menuframe,text='Tequila & Orange Juice',bg="white").place(x=20,y=160)

#SD Button

SDb=Button(menuframe, justify = CENTER, bd=3,command=ButtonPress)
#SDpic = PhotoImage(file="screwdriver.gif")
SDb.config(width="130",height="130",compound=TOP)
SDb.config(text="Screwdriver", font=Drinkfont)
SDb.place(x=190,y=12)
SDingred = Label(menuframe,text='Vodka & Orange Juice',bg="white").place(x=190,y=160)

#WR button 

WRb=Button(menuframe, justify = CENTER, bd=3,command=ButtonPress)
#WRpic = PhotoImage(file="whiterussian.gif")
WRb.config(width="130",height="130",compound=TOP)
WRb.config(text="White Russian", font=Drinkfont)
WRb.place(x=360,y=10)
WRingred = Label(menuframe,text='Vodka, Kaluha, Milk',bg="white").place(x=360,y=158)

#WC Button

WCb=Button(menuframe, justify = CENTER, bd=3,command=ButtonPress)
#WCpic = PhotoImage(file="whiskeycoke.gif")
WCb.config(width="130",height="130",compound=TOP)
WCb.config(text="Whiskey Coke", font=Drinkfont)
WCb.place(x=540,y=10)
WCingred = Label(menuframe,text='Whiskey & Coke',bg="white").place(x=540,y=158)

#VS Button

VSb=Button(menuframe, justify = CENTER, bd=3,command=ButtonPress)
#VSpic = PhotoImage(file="vodkasprite.gif")
VSb.config(width="130",height="130",compound=TOP)
VSb.config(text="Vodka Sprite", font=Drinkfont)
VSb.place(x=540,y=175)
VSingred = Label(menuframe,text='Vodka & Sprite',bg="white").place(x=540,y=323)

#GT Button

GTb=Button(menuframe, justify = CENTER, bd=3,command=ButtonPress)
#GTpic = PhotoImage(file="gintonic.gif")
GTb.config(width="130",height="130",compound=TOP)
GTb.config(text="Gin & Tonic", font=Drinkfont)
GTb.place(x=360,y=175)
GTingred = Label(menuframe,text='Gin & Tonic Water',bg="white").place(x=360,y=323)

#LI Button

LIb=Button(menuframe, justify = CENTER, bd=3,command=ButtonPress)
#LIpic = PhotoImage(file="longisland.gif")
LIb.config(width="130",height="130",compound=TOP)
LIb.config(text="Long Island", font=Drinkfont)
LIb.place(x=360,y=340)
LIingred = Label(menuframe,text='Vodka, Tequila, Rum, \n Triple Sec, Sweet & Sour',bg="white").place(x=352,y=487)

#VR Button

VRb=Button(menuframe, justify = CENTER, bd=3,command=VR)
#VRpic = PhotoImage(file="vodkaRedbull.gif")
VRb.config(width="130",height="130",compound=TOP)
VRb.config(text="Vodka Redbull", font=Drinkfont)
VRb.place(x=520,y=340)
VRingred = Label(menuframe,text='Vodka & Redbull',bg="white").place(x=520,y=487)

#MH Button

MHb=Button(menuframe, justify = CENTER, bd=3,command=MH)
#MHpic = PhotoImage(file="Manhattan.gif")
MHb.config(width="130",height="130",compound=TOP)
MHb.config(text="Manhattan", font=Drinkfont)
MHb.place(x=190,y=340)
MHingred = Label(menuframe,text='Whiskey, Vermouth, Cherry',bg="white").place(x=190,y=487)

#CM Button

CMb=Button(menuframe, justify = CENTER, bd=3,command=CM)
#CMpic = PhotoImage(file="Cosmo.gif")
CMb.config(width="130",height="130",compound=TOP)
CMb.config(text="Cosmopolitan", font=Drinkfont)
CMb.place(x=30,y=340)
CMingred = Label(menuframe,text='Vodka, Orange Liqueur, \n Cranberry Juice',bg="white").place(x=30,y=487)

#TC Button

TCb=Button(menuframe, justify = CENTER, bd=3,command=TC)
#TCpic = PhotoImage(file="tomcollins.gif")
TCb.config(width="130",height="130",compound=TOP)
TCb.config(text="Tom Collins", font=Drinkfont)
TCb.place(x=190,y=180)
TCingred = Label(menuframe,text='Gin, Lemon Juice, Club Soda',bg="white").place(x=190,y=320)

#TC Button

MTb=Button(menuframe, justify = CENTER, bd=3,command=MT)
#MTpic = PhotoImage(file="maitai.gif")
MTb.config(width="130",height="130",compound=TOP)
MTb.config(text="Mai Tai", font=Drinkfont)
MTb.place(x=20,y=180)
MTingred = Label(menuframe,text='Rum, Triple Sec, Amaretto,\n Lime Juice',bg="white").place(x=20,y=320)




dgui.mainloop()
